<?php include('header.php'); ?>

<section role="region" aria-labelledby="heading" class="contenedor principal">
	<article>
		<h1>Teléfonos de emergencia</h1>
		<p>
			Algunos servicios médicos disponibles en caso de presentar síntomas mencionados o tener dudas son los siguientes:
		</p>
		<ul>
			<li>
				<strong>Medicina a distancia de la Secretaría de Salud</strong><br/>
				5132-0909
			</li>
			<li>
				<strong>Emergencias mayores</strong><br/>
				5683-2222
			</li>
			<li>
				<strong>ERUM</strong><br/>
				5588-7418
			</li>
			<li>
				<strong>Dirección General de Información en Salud</strong><br/>
				5514-5989<br/>
				5514-5238
			</li>
			<li>
				<strong>Bayer de México</strong><br/>
				5728-3000 (Conmutador)
			</li>
		</ul>

	</article>

</section>

<!-- * Sless lo pone en cada título principal
Al final de cada apartado siempre pone Si tiene alguna duda, consulte a su médico -->

<?php include('footer.php'); ?>