<?php include('header.php'); ?>

<section role="region" aria-labelledby="heading" class="contenedor principal">
	<article>
		<h1>¿Para qué se usa Microgynon&reg; CD?</h1>
		<div class="columna_dos_tercios">
			<p>
				Microgynon® CD es un anticonceptivo oral. Se utiliza principalmente para prevenir el embarazo y en tratamientos relacionados con la menstruación. Tu médico puede recomendarte Microgynon® CD por otra razón.<br/>
				Pregúntale si tienes alguna duda.
			</p>
			<h2>
				¿Qué hay en este instructivo?
			</h2>
			<p>
				Este instructivo encontrarás información precisa y necesaria para usar correctamente Microgynon &reg;. Te ayudará a entender cómo usarlo y sus efectos secundarios.
			</p>
			<p>
				Te recomendamos leer el instructivo en el siguiente orden:
			</p>
			<ul>
				<li>
					<a href="antes-de-tomar-microgynon-cd" title="Antes de tomar Microgynon&reg;">Antes de tomar Microgynon&reg; CD</a>
				</li>
				<li>
					<a href="como-tomar-microgynon-cd" title="¿Cómo tomar Microgynon&reg; CD?">¿Cómo tomar Microgynon&reg; CD? </a>
				</li>
				<li>
					<a href="que-hacer-si-olvido-tomar-microgynon-cd" title="¿Qué hacer">¿Qué hacer si olvidas tomar Microgynon&reg; CD?</a>
				</li>
				<li>
					<a href="efectos-secundarios-microgynon-cd" title="Efectos secundarios de Microgynon&reg; CD">Efectos secundarios de Microgynon&reg; CD</a>
				</li>
			</ul>
			<p>
				Y para conocer más información:
			</p>
			<ul>
				<li>
					<a href="bayer-mexico" title="Bayer de México">Bayer de México</a>
				</li>
				<li>
					<a href="telefonos-emergencia" title="Teléfonos de emergencia">Teléfonos de emergencia</a>
				</li>
			</ul>
		</div>
		<div class="columna_tercio">
			<figure class="ilustracion">
				<img src="imgs/mujer-1.png" alt="Ilustración">
			</figure>
		</div>
	</article>
</section>

<?php include('footer.php'); ?>