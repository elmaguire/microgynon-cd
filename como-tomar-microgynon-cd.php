<?php include('header.php'); ?>

<section role="region" aria-labelledby="heading" class="contenedor principal">
	<article>
		<h1>¿Cómo tomar Microgynon&reg; CD?</h1>
		<h2>
			Conoce el envase
		</h2>
		<div class="bloque">
			<p>
				** La fecha de caducidad
			</p>
			<figure>
				
			</figure>
		</div>
		<div class="bloque">
			<p>
				El envase protector de las grageas tiene dos caras. En el frente puedes ver las 28 grageas (21 de color y 7 blancas) a través de burbujas transparentes. La posterior está divida en dos secciones: una verde y una roja.
			</p>
			<figure class="doble">
				<img src="imgs/blister-2.png" alt="Blister">
				<img src="imgs/blister.png" alt="Blister">
			</figure>
		</div>
		<div class="bloque">
			<div class="columna">
				<p>
					Las secciones verde y roja tienen marcados los días de la semana y el orden a seguir para tomar Microgynon® CD.
				</p>
			</div>
			<div class="columna_tercio">
				<figure class="dias-orden">
					<img src="imgs/blister.png" alt="Blister">
				</figure>
			</div>
		</div>
		<div class="bloque">
			<div class="columna">
				<p>
					Para sacar una gragea oprime la burbuja correspondiente al día de la semana hasta que salga por el lado opuesto del envase.
				</p>
			</div>
			<div class="columna_tercio">
				<figure class="oprime">
					<img src="imgs/gragea.png" alt="Gragea" class="gragea">
					<img src="imgs/blister-2.png" alt="Blister">
					<span class="micro2-hand-o-up"></span>
				</figure>
			</div>
		</div>
		
		<h2>
			Si en el mes anterior no usabas otros anticonceptivos con hormonas:
		</h2>
		<div class="bloque">
			<div class="columna_tercio">
				<p>
					Comienza a tomar Microgynon® CD el mismo día en que empieza tu menstruación. Toma la gragea del área roja correspondiente al día de la semana, a la hora del desayuno o de la cena.
				</p>
				<p>
					<span class="rosa">
						Por ejemplo: si tu menstruación empieza el lunes toma la gragea marcada como “Lun”.
					</span>
				</p>
			</div>
			<div class="columna">
				<figure class="calendario">
					<div class="col_calendario">
						<span class="micro-downleft"></span>
						<img src="imgs/calendario.png" alt="Calendario">
					</div>
					<div class="col_calendario">
						<span class="micro-downleft"></span>
						<img src="imgs/blister.png" alt="Blister">
					</div>
				</figure>
			</div>
		</div>
		<div class="bloque">
			<div class="columna">
				<p>
					Toma la siguiente gragea de Microgynon® CD de acuerdo al orden de las flechas al día siguiente. Repite la dosis diaria hasta terminar el envase.
				</p>
				<p>
					Recuerda tomar una gragea todos los días siempre a la misma hora para conservar el efecto anticonceptivo. 
				</p>
			</div>
			<div class="columna_tercio">
				<figure class="secuencia">
					<img src="imgs/blister.png" alt="Blister">
					<span class="micro-right"></span>
				</figure>
			</div>
		</div>
		<div class="bloque">
			<p>
				Usa algún método anticonceptivo no hormonal adicional durante los primeros 14 días de la primera caja. 
			</p>
			<p>
				Cada caja de Microgynon® CD dura un ciclo menstrual.
			</p>
		</div>
		<div class="bloque">
				<h3>
					Continua con el tratamiento:
				</h3>
				<p>
					Una vez terminada la primer caja debes empezar una nueva al día siguiente para evitar quedar embarazada.
				</p>
		</div>
		<div class="bloque">
			<div class="columna">
				<p>
					Toma la primer pastilla de la caja nueva. Comienza siempre por la sección roja del envase de Microgynon® CD.
				</p>
				<p>
					La menstruación se presentará los días en que tomas las grageas de la sección marcada en rojo.
				</p>
			</div>
			<div class="columna_tercio">
				<figure class="donde-empezar">
					<span class="micro-down"></span>
					<img src="imgs/blister.png" alt="Blister">
				</figure>
			</div>
		</div>
		<div class="bloque">
			<h2>
				Si antes usabas otros anticonceptivos con hormonas:
			</h2>
			<p>
				Consulta a tu médico si utilizabas otro anticonceptivos con hormonas o tienes alguna duda. 
			</p>
		</div>

		<div class="bloque">
			<h2>
				¿Cómo guardar Microgynon&reg; CD?
			</h2>
			<p>
				Guarda Microgynon&reg; CD en un lugar en donde recuerdes fácilmente de tomártelas. No necesita condiciones especiales, solamente conserva las grageas en el envase original.
			</p>
		</div>

		<h2>
			Al terminar una caja de Microgynon&reg; CD
		</h2>

		<p>
			Conserva la caja y este instructivo. Contienen información de gran utilidad.
		</p>
		<p>
			No te sobrarán grageas si completas el tratamiento de manera correcta. 
		</p>
		<p>
			Tira las grageas sobrantes junto con el envase en los depósitos especiales ubicados en farmacias, hospitales o clínicas, nunca en la basura. Así ayudarás al medio ambiente.
		</p>
		<p>
			Consulta a tu médico si tienes alguna duda.
		</p>		

	</article>

</section>


<?php include('footer.php'); ?>