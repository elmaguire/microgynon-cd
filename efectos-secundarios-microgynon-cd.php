<?php include('header.php'); ?>

<section role="region" aria-labelledby="heading" class="contenedor principal">
	<article>
		<h1>Efectos secundarios de Microgynon&reg; CD</h1>
		
		<h3>
			Mientras estás tomando Microgynon® CD pueden presentarse algunos malestares como:
		</h3>

		<ul>
			<li>Náuseas.</li>
			<li>Dolor de estómago.</li>
			<li>Aumento de peso.</li>
			<li>Dolor de cabeza ligero.</li>
			<li>Cambios de humor.</li>
			<li>Mucha sensibilidad y dolor en los senos.</li>
		</ul>

		<p>
			<span class="rosa"><span class="micro-asterisk2"></span> Pero no debes alarmarte ni suspender el tratamiento.</span>
		</p>

		<hr/>

		<h3>
			Consulta a tu médico lo antes posible si presentas alguno de los siguientes síntomas:
		</h3>

		<ul>
			<li>Dolor de cabeza intenso.</li>
			<li>Alteraciones en la vista.</li>
			<li>Dolor intenso en el pecho.</li>
			<li>Sientes que te falta el aire.</li>
			<li>Tu piel se pone amarilla.</li>
			<li>Dolor en las piernas.</li>
		</ul>

		<p>
			Consulta a tu médico si tienes otros síntomas o alguna duda.
		</p>

	</article>

</section>

<!-- * Sless lo pone en cada título principal
Al final de cada apartado siempre pone Si tiene alguna duda, consulte a su médico -->

<?php include('footer.php'); ?>