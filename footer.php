</main>
<footer role="contentinfo">
	<div class="contenedor">
		<figure class="logo">
			<img src="imgs/logo.png" alt="Microgynon">
		</figure>
		<div class="ubicacion">
			Estás leyendo: <span class="js-seccion secc-actual"></span>.
			<br/>
			<span class="js-sig-seccion"></span>
		</div>
		<figure class="bayer">
			<img src="imgs/logo-bayer.png" alt="Bayer">
		</figure>
	</div>
</footer>
<script src="js/funciones-min.js"></script>

</body>
</html>