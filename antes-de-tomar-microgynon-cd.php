<?php include('header.php'); ?>

<section role="region" aria-labelledby="heading" class="contenedor principal">
	<article>
		<h1>Antes de tomar Microgynon&reg; CD</h1>

		<div class="columna_dos_tercios">
			<h2>
				Presentaciones de Microgynon&reg;
			</h2>
			<p>
				Existen dos presentaciones de este medicamento, Microgynon® y Microgynon® CD. 
			</p>
			<p>
				<strong>Microgynon&reg;</strong> contiene 21 grageas en cada caja. Al terminar las pastillas de una caja se dejan pasar siete días sin tomar el medicamento y al octavo se comienza la siguiente caja.
			</p>
			<p>
				<strong>Microgynon&reg; CD</strong> contiene 28 grageas en cada caja, 21 grageas de color y 7 blancas. A día siguiente de tomar la última gragea de una caja se toma la primera gragea de la siguiente sin interrupción.
				<span class="rosa">
					Este instructivo corresponde a esta presentación. 
				</span>
			</p>
			<p>
				Distingamos estas dos presentaciones. No son intercambiables. Consulta a tu médico si tienes alguna pregunta.
			</p>
			<h2>
				Contraindicaciones
			</h2>
			<h3>
				<span class="importante">No</span> tomes Microgynon® CD si:
			</h3>
			<ul>
				<li>
					Estás embarazada o sospechas estarlo.
				</li>
				<li>
					Tuviste un bebé hace menos de 6 semanas.
				</li>
				<li>
					Estás amamantando.
				</li>
				<li>
					Si tienes presión arterial alta y no está controlada.
				</li>
				<li>
					Tienes diabetes con complicaciones.
				</li>
				<li>
					Tienes riesgo de padecer o has padecido formación de coágulos en las venas o arterias.
				</li>
				<li>
					Si padeces de migraña acompañada de problemas en la visión, dificultad en el habla, debilidad o entumecimiento en alguna parte del cuerpo.
				</li>
				<li>
					Si padeces alguna enfermedad del hígado.
				</li>
				<li>
					Tienes, has padecido o sospechas tener cáncer de mama.
				</li>
				<li>
					Si eres alérgica a cualquiera de los componentes de Microgynon® CD.
				</li>
			</ul>

			<p>
				Suspende inmediatamente el tratamiento si aparece cualquiera de estas situaciones por primera vez mientras tomas Microgynon® CD. Consulta a tu médico.
			</p>

			<h3>
				Consulta a tu médico antes de tomar Microgynon® CD si:
			</h3>
			<ul>
				<li>
					Padeces de:
					<ul>
						<li>Problemas de circulación.</li>
						<li>Diabetes.</li>
						<li>Dolores de cabeza seguidos y muy fuertes.</li>
						<li>Enfermedad inflamatoria del intestino.</li>
						<li>Enfermedades del hígado.</li>
					</ul>
				</li>
				<li>
					Padeciste depresión.
				</li>
				<li>
					Crees estar embarazada.
				</li>
				<li>
					Tienes más de 40 años.
				</li>
				<li>
					Fumas, especialmente si tienes más de 35 años.
				</li>
				<li>
					Tienes sangrado que no es tu menstruación.
				</li>
				<li>
					Estás en tratamiento o tomando algún otro medicamento.
				</li>
			</ul>
		</div>
		<div class="columna_tercio">
			<figure class="ilustracion">
				<img src="imgs/mujer-2.png" alt="Ilustración">
			</figure>
		</div>
	</article>
</section>

<?php include('footer.php'); ?>