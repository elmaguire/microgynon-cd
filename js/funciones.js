// JS para Microgynon

// Función para identificar si un elemento tiene una clase o no
function tiene_clase(el, clase) {
	return (' ' + el.className + ' ').indexOf(' ' + clase + ' ') > -1;
}

function btn_todo()
{
	var todo = document.querySelector('.js-todo');
	var apagador = "cerrado";

	if (todo){

		todo.onclick = function () {

			var opcion = document.getElementsByClassName('js-opcion');

			if ( apagador == "cerrado") {
				todo.innerText = "Contraer todo el contenido";
				apagador = "abierto";

				Array.prototype.forEach.call(opcion, function(el){
					var clase = 'activo';
					var op = el.getAttribute('data-opcion');
					var respuesta = document.querySelector( '#'+ op );

					if ( !tiene_clase(el, clase) )
					{
						respuesta.classList.add("visible");
						el.classList.add(clase);
					}
				});
			} else if ( apagador == "abierto" ) {
				todo.innerText = "Mostrar todo el contenido";
				apagador = "cerrado";

				Array.prototype.forEach.call(opcion, function(el){
					var clase = 'activo';
					var op = el.getAttribute('data-opcion');
					var respuesta = document.querySelector( '#'+ op );

					if ( tiene_clase(el, clase) )
					{
						respuesta.classList.remove("visible");
						el.classList.remove(clase);
					}
				});
			}

		};

	}
}

function cada_opcion()
{
	var opcion = document.getElementsByClassName('js-opcion');

	Array.prototype.forEach.call(opcion, function(el){
		el.onclick = function() {
			// Variable string activo
			var clase = 'activo';
			// Obtener el valor del data-option
			var op = this.getAttribute('data-opcion');
			// Obtener el elemento que tenga ese data-option como id
			var respuesta = document.querySelector( '#'+ op );

			// Controlador para hacer "toggle" si tiene clase activo o no
			if ( tiene_clase(this, clase) ){
				respuesta.classList.remove("visible");
				this.classList.remove(clase);
			} else {
				respuesta.classList.add("visible");
				this.classList.add(clase);
			}


		};
	});
}

// Agregar clase Activo al botón del menú de la sección actual
function menuActivo(){
	var url = window.location.pathname,
	urlRegExp = new RegExp(url.replace(/\/$/,'') + "$");
	if( url != '/'){

		var links = document.querySelectorAll('nav ul li a');
		Array.prototype.forEach.call(links, function(el){
			if(urlRegExp.test(el.href.replace(/\/$/,''))){
				el.classList.add("activo");
			}
		});

	}
}

// Identificar la sección en la que nos encontramos e imprimir el nombre y link de la siguiente sección
function seccion () {
	var secc = document.querySelector("h1");
	var js_secc = document.querySelector(".js-seccion");
	var nom_secc = secc.innerText;
	var cont_secc = secc.textContent;
	if (nom_secc) {
		js_secc.innerText = nom_secc;
	} else {
		nom_secc = cont_secc.replace(/\s+/g, " ").trim();
		js_secc.textContent = nom_secc;
	}

	var secciones = [
		'¿Para qué se usa Microgynon® CD?',
		'Antes de tomar Microgynon® CD',
		'¿Cómo tomar Microgynon® CD?',
		'¿Qué hacer si olvidas tomar Microgynon® CD?',
		'Efectos secundarios de Microgynon® CD',
		'Bayer de México',
		'Teléfonos de emergencia'
	];
	var links = [
		'index.php',
		'antes-de-tomar-microgynon-cd',
		'como-tomar-microgynon-cd',
		'que-hacer-si-olvido-tomar-microgynon-cd',
		'efectos-secundarios-microgynon-cd',
		'bayer-mexico',
		'telefonos-emergencia'
	];
	for(var i=0,j=secciones.length-1;i<j;i++)
	{
		if (secciones[i] == nom_secc) {
			var js_sig_secc = document.querySelector(".js-sig-seccion");
			js_sig_secc.innerHTML = "Continúa: <a href='"+ links[i+1] +"'>"+ secciones[i+1] +"</a>";
		}
	}
}

// Ejecutar funciones cuando el doc esté listo
document.addEventListener('DOMContentLoaded', function(){
	btn_todo();
	cada_opcion();
	menuActivo();
	seccion();
});