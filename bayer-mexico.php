<?php include('header.php'); ?>

<section role="region" aria-labelledby="heading" class="contenedor principal">
	<article>
		<h1 id="titulo">Bayer de México</h1>
		<p>
			Microgynon® CD es fabricado en México por:
		</p>
		<p>
			BAYER DE MÉXICO, S. A. de C. V.<br/>
			Ojo de Agua, s/n, Ixtaczoquitlán<br/>
			94450 Veracruz, México<br/>
			Reg. Núm. 82813, SSA IV<br/>
			&reg;Marca Registrada.
		</p>
		<p>
			<strong>Contacto de la empresa:</strong><br/>
			<a href="http://www.bayer.com.mx" title="Bayer de México">www.bayer.com.mx</a><br/>
			Tel.: (55) 5728-3000 (Conmutador)<br/>
			Fax: (55) 5728-3111
		</p>
	</article>
</section>


<?php include('footer.php'); ?>