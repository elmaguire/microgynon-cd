<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Microgynon</title>
	<link rel="shortcut icon" href="imgs/favicon.ico" />
	<!-- Overlock -->
	<link href='http://fonts.googleapis.com/css?family=Overlock:700' rel='stylesheet' type='text/css'>
	<!-- PT Sans -->
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="estilos.css">
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>

<body>
<header role="banner">
	<div class="contenedor">
		<figure id="logo">
			<a href="http://madic.pw/microgynon-cd">
				<img src="imgs/logo.png" alt="Microgynon">
				<img src="imgs/picto.png" alt="Microgynon">
			</a><!-- 
			 --><figcaption>
				<strong>Anticonceptivo oral.</strong>
				<span class="medico">Fórmula: Levonorgestrel (0.15 mg) - Etinilestradiol (0.03 mg).</span>
				<span class="cont">Caja con 28 grageas (21 grageas de color y 7 grageas blancas).</span>
			</figcaption>
		</figure>
		<div class="descripcion">
			<p>
				Instructivo de información sobre Microgynon® CD
			</p>
		</div>
	</div>
</header>

<nav role="navigation">
	<ul>
		<li>
			<a href="http://madic.pw/microgynon-cd" title="¿Para qué se usa Microgynon&reg; CD?">¿Para qué se usa Microgynon&reg; CD?</a>
		</li>
		<li>
			<a href="antes-de-tomar-microgynon-cd" title="Antes de tomar Microgynon&reg;">Antes de tomar Microgynon&reg; CD</a>
		</li>
		<li>
			<a href="como-tomar-microgynon-cd" title="¿Cómo tomar Microgynon&reg; CD?">¿Cómo tomar Microgynon&reg; CD? </a>
		</li>
		<li>
			<a href="que-hacer-si-olvido-tomar-microgynon-cd" title="¿Qué hacer">¿Qué hacer si olvidas tomar Microgynon&reg; CD?</a>
		</li>
		<li>
			<a href="efectos-secundarios-microgynon-cd" title="Efectos secundarios de Microgynon&reg; CD">Efectos secundarios de Microgynon&reg; CD</a>
		</li>
		<li>
			<a href="bayer-mexico" title="Bayer de México">Bayer de México</a>
		</li>
		<li>
			<a href="telefonos-emergencia" title="Teléfonos de emergencia">Teléfonos de emergencia</a>
		</li>
	</ul>
</nav>
<main role='main' id="main">