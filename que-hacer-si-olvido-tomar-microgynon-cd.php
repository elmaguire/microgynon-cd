<?php include('header.php'); ?>

<section role="region" aria-labelledby="heading" class="contenedor principal olvido">
	<article>
		<h1 id="titulo">¿Qué hacer si olvidas tomar Microgynon&reg; CD?</h1>
		<p>
			¿Olvidaste tomar Microgynon&reg; CD? No te preocupes, da click en las siguientes opciones para descubrir qué debes hacer en cada uno de los casos.
		</p>
		<button class="js-todo todo">Mostrar todo el contenido</button>
		<button class="js-opcion btn-opcion" data-opcion="uno">
			<h3>
				Si olvidaste tomar una gragea y no han pasado más de 12 horas:
				<span class="micro-uniF47A"></span>
			</h3>
		</button>

		<div id="uno" class="respuestas">
			
			<p>
				Toma la gragea que se te olvidó inmediatamente y continúa tomando las siguientes grageas a la hora que acostumbras.
				<span class="rosa">
					<span class="micro-asterisk2"></span> No hay problema si tomas dos grageas el mismo día.
				</span>
			</p>
		</div>

		<button class="js-opcion btn-opcion" data-opcion="dos">
			<h3>
				Si olvidaste tomar una gragea y ya pasaron más de 12 horas:
				<span class="micro-uniF47A"></span>
			</h3>
		</button>

		<div id="dos" class="respuestas">
			
			<p>
				Ubica en qué semana del ciclo te encuentras:
				<span class="rosa">
					<span class="micro-asterisk2"></span> Recuerda que cada ciclo empieza el primer día de tu menstruación.
				</span>
			</p>


			<button class="js-opcion btn-opcion" data-opcion="sem_uno">
				<h4>
					Estás en la semana 1 de tu ciclo:<span class="micro-uniF47A"></span>
				</h4>
			</button>

			<div id="sem_uno" class="respuestas">
				<ul>
				<li>
					Toma la última gragea olvidada enseguida, después continúa tomando las grageas a la hora que acostumbras.
					<span class="rosa"><span class="micro-asterisk2"></span> Recuerda que no hay problema si tomas dos grageas el mismo día.</span>
				</li>
				<li>
					Utiliza un método anticonceptivo de barrera, por ejemplo, condón durante los siguientes 7 días.
				</li>
				<li>
					Existe la posibilidad de haber quedado embarazada si tuviste relaciones en los 7 días anteriores.<br/><strong>Consulta a tu médico</strong>.
				</li>
				</ul>
			</div>

			<button class="js-opcion btn-opcion" data-opcion="sem_dos">
				<h4>
					Estás en la semana 2 de tu ciclo:<span class="micro-uniF47A"></span>
				</h4>
			</button>
			
			<div id="sem_dos" class="respuestas">
				<ul>
					<li>
						Toma la última gragea olvidada enseguida, después continúa tomando las grageas a la hora acostumbrada.
						<span class="rosa">
							<span class="micro-asterisk2"></span>Recuerda, no hay problema si tomas dos grageas el mismo día.
						</span>
					</li>
				</ul>
			</div>

			<button class="js-opcion btn-opcion" data-opcion="sem_tres">
				<h4>
					Estás en la semana 3 de tu ciclo:<span class="micro-uniF47A"></span>
				</h4>
			</button>

			<div id="sem_tres" class="respuestas">

					<ul>
						<li>
							Utiliza el método anterior.
						</li>
						<li>
							También puedes dejar de tomar las grageas de color hasta completar 7 días y empezar un nuevo envase de Microgynon® CD.
						</li>
						<li>
							Empezará tu menstruación durante estos 7 días. Si no empieza es probable que estés embarazada.<br/><strong>Consulta a tu médico</strong>.
						</li>
					</ul>
			</div>
			<div class="nota rosa">
				<p>
					<span class="micro-asterisk2"></span> La diarrea y el vómito intensos pueden interferir con la efectividad de las grageas. <span class="importante">En esos casos debes utilizar algún método anticonceptivo de barrera</span>, por ejemplo condón.
				</p>
			</div>
		</div>

		<button class="js-opcion btn-opcion" data-opcion="tres">
			<h3>
				Si olvidaste tomar hasta 7 grageas:
				<span class="micro-uniF47A"></span>
			</h3>
		</button>

		<div id="tres" class="respuestas">
			
			<p>
				Ubica en qué semana del ciclo te encuentras:
				<span class="rosa">
					<span class="micro-asterisk2"></span> Recuerda que cada ciclo empieza el primer día de tu menstruación.
				</span>
			</p>

			<button class="js-opcion btn-opcion" data-opcion="sem_dos_dos">
				<h4>
					Estás en la semana 2 de tu ciclo:<span class="micro-uniF47A"></span>
				</h4>
			</button>
			
			<div id="sem_dos_dos" class="respuestas">
				<ul>
					<li>
						<strong>Utiliza un método anticonceptivo de barrera</strong>, como el condón, durante los siguientes siete días en caso de haber olvidado tomar más de dos grageas.
					</li>
				</ul>
			</div>

			<button class="js-opcion btn-opcion" data-opcion="sem_tres_dos">
				<h4>
					Estás en la semana 3 de tu ciclo:<span class="micro-uniF47A"></span>
				</h4>
			</button>

			<div id="sem_tres_dos" class="respuestas">
					<ul>
						<li>
							Toma la última gragea olvidada enseguida, después continúa tomando las grageas a la hora acostumbrada.
						</li>
						<li>
							No tomes las grageas blancas. Tiralas. 
						</li>
						<li>
							Empieza un nuevo envase de Microgynon® CD en su lugar.
						</li>
						<li>
							Utiliza un método anticonceptivo de barrera, como el condón, durante los siguientes 7 días.
							<span class="rosa">
							<span class="micro-asterisk2"></span> En este ciclo no se presentará la menstruación, pero puedes tener un ligero sangrado.
						</li>
					</ul>
			</div>
			<div class="nota rosa">
				<p>
					<span class="micro-asterisk2"></span> Recuerda que entre más pastillas olvides es más probable que quedes embarazada. Si olvidas tomar más de 7 pastillas seguidas se perderá la protección anticonceptiva.
				</p>
			</div>
		</div>

	</article>


	</article>

</section>


<?php include('footer.php'); ?>